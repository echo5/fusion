// ==== UTILITIES ==== //

var gulp        = require('gulp'),
    plugins     = require('gulp-load-plugins')({ camelize: true }),
    del         = require('del'),
    replace     = require('gulp-replace'),
    config      = require('../../gulpconfig').utils,
    wpPot       = require('gulp-wp-pot');

// Totally wipe the contents of the `dist` folder to prepare for a clean build; additionally trigger Bower-related tasks to ensure we have the latest source files
gulp.task('utils-wipe', function() {
  return del(config.wipe);
});

// Clean out junk files after build
gulp.task('utils-clean', ['build', 'utils-wipe'], function() {
  return del(config.clean);
});

// Copy files from the `build` folder to `dist/[project]`
gulp.task('utils-dist', ['utils-replace'], function() {

});

gulp.task('utils-copy', ['utils-clean', 'utils-translate'], function() {
  return gulp.src(config.dist.src)
  .pipe(gulp.dest(config.dist.dest));
});

gulp.task('utils-replace', ['utils-copy'], function() {
  return gulp.src(config.dist.destReplace)
  .pipe(replace('__s__', config.dist.project))
  .pipe(replace('_S_', config.dist.name + '_'))
  .pipe(replace('_s_', config.dist.project + '_'))
  .pipe(replace('_s-', config.dist.project + '-'))
  .pipe(replace('\'_s\'', '\'' + config.dist.project + '\''))
  .pipe(replace('"_s"', '\'' + config.dist.project + '\''))
  .pipe(replace(' _s', ' ' + config.dist.project))
  .pipe(gulp.dest(config.dist.dest));
});


gulp.task('utils-translate', function () {
  return gulp.src(config.translate.src)
  .pipe(wpPot( {
      domain: '_s',
      package: '_S_',
      relativeTo: 'build'
  } ))
  .pipe(gulp.dest(config.translate.dest + config.translate.slug + '.pot'));
});


// Find and replace all theme name instances
// gulp.task('utils-replace', function(){
//   gulp.src(config.dist.src)
//     .pipe(replace('_s_', conf.dist.project))
//     .pipe(gulp.dest(config.dist.dest));
// });