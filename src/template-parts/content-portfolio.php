<?php
/**
 * Template part for displaying portfolio content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

?>
<?php get_template_part( 'template-parts/portfolio/portfolio', _s_get_portfolio_grid_style() ); ?>
