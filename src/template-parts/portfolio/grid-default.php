<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="featured mb-3">
		<a href="<?php echo the_permalink(); ?>">
			<?php the_post_thumbnail($GLOBALS['image_size']); ?>
		</a>
	</div><!-- .featured -->

	<a href="<?php echo the_permalink(); ?>">
		<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
	</a>
	<div class="categories">
		<?php echo implode(', ', _s_get_portfolio_categories()); ?>
	</div>

</article><!-- #post-## -->