<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<header class="entry-header page-header">
			<div class="categories">
				<?php echo implode(', ', _s_get_portfolio_categories()); ?>
			</div>
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<div class="separator"></div>
		</header><!-- .entry-header -->
		
		<div class="row">
			<div class="col-md-7">
				<?php get_template_part('template-parts/portfolio/details'); ?>
			</div>

			<div class="col-md-5">
				<div class="entry-content">
					<?php
						the_content();
					?>
				</div><!-- .entry-content -->
			</div>
		</div>

		<div class="featured">
			<?php
			if ( get_post_gallery() ) :
			    echo get_post_gallery();
	  	else :
	  		the_post_thumbnail('full');
			endif; 
			?>
		</div><!-- .featured -->

		<footer>
			<?php the_post_navigation(); ?>
		</footer>
	</div>

</article><!-- #post-## -->