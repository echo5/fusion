<div class="post-meta">
	<div class="author">
		<span class="label h5"><?php esc_html__( 'Author:', '_s'); ?></span> <?php the_author(); ?>
	</div>
	<div class="date">
		<span class="label h5"><?php esc_html__( 'Date:', '_s'); ?></span> <?php the_date(_s_default_date_format()); ?>
	</div>
	<div class="page-header-categories">
		<span class="label h5"><?php esc_html__( 'Categories:', '_s'); ?></span> <?php the_category(', '); ?>
	</div>
</div>