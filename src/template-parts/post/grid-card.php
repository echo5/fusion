<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="row">
		<div class="featured col-md-8">
			<?php the_post_thumbnail($GLOBALS['image_size']); ?>
		</div><!-- .featured -->

		<div class="col-md-5 card-col">
			<div class="card mt-5">
				<div class="card-block">
					<a href="<?php echo the_permalink(); ?>">
						<?php the_title( '<h3 class="entry-title h2 mb-5">', '</h3>' ); ?>
					</a>
					<div class="post-meta spacer-before">
						<span class="author"><?php the_author(); ?></span>
						<span class="date"><?php the_date(_s_default_date_format()); ?></span>
					</div>

					<div class="categories">
						<?php the_category(', '); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

</article><!-- #post-## -->