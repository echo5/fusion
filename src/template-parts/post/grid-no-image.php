<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<div class="col-md-6 title-column">
			<a href="<?php the_permalink(); ?>">
				<?php the_title( '<h2 class="h2 entry-title mb-2">', '</h2>' ); ?>
			</a>
			<div class="post-meta">
				<span class="author"><?php the_author(); ?></span>
				<span class="date"><?php the_date(_s_default_date_format()); ?></span>
			</div>
		</div>

		<div class="col-md-6 excerpt-column">
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" class="read-more"><?php esc_html_e('Read More', '_s'); ?></a>
		</div>
	</div>

</article><!-- #post-## -->