<?php

if (_s_is_kirki_active()) {
	
	/**
	 * Luxe Option class
	 */
	require_once get_template_directory() . '/inc/options/luxe-option.php';

	/**
	 * Add the configuration.
	 * This way all the fields using the 'luxe_options' ID
	 * will inherit these options
	 */
	LuxeOption::add_config( 'luxe_options', array(
	    'capability'    => 'edit_theme_options',
	    'option_type'   => 'theme_mod',
	) );

	/**
	 * Disable Kirki loader
	 */
	add_filter( 'kirki/config', function( $config ) {
	    $config['disable_loader'] = true;
	    return $config;
	} );

	/**
	 * Add sections
	 */
	require_once get_template_directory() . '/inc/options/header/header-base.php';
	require_once get_template_directory() . '/inc/options/header/header-dark.php';
	require_once get_template_directory() . '/inc/options/header/header-light.php';
	require_once get_template_directory() . '/inc/options/nav/nav-base.php';
	require_once get_template_directory() . '/inc/options/nav/nav-dark.php';
	require_once get_template_directory() . '/inc/options/nav/nav-light.php';
	require_once get_template_directory() . '/inc/options/body.php';
	require_once get_template_directory() . '/inc/options/typography.php';
	require_once get_template_directory() . '/inc/options/buttons.php';
	require_once get_template_directory() . '/inc/options/forms.php';
	require_once get_template_directory() . '/inc/options/post-types.php';
	require_once get_template_directory() . '/inc/options/loading.php';
	require_once get_template_directory() . '/inc/options/elementor.php';
	require_once get_template_directory() . '/inc/options/maps.php';
	require_once get_template_directory() . '/inc/options/advanced.php';

	/**
	 * Add admin css styles to customizer
	 */
	// function _s_custom_customize_enqueue() {
	//     wp_enqueue_style('luxe-admin', Assets\luxe_asset_path('styles/customizer.css'));
	// }
	// add_action( 'customize_controls_enqueue_scripts', '_s_custom_customize_enqueue' );
}