<?php
namespace Elementor;

if (_s_is_elementor_active()) {

	require_once get_template_directory() . '/inc/widgets/icons.php';
	require_once get_template_directory() . '/inc/widgets/animation-controls.php';
	require_once get_template_directory() . '/inc/widgets/position-controls.php';

	function _s_map_meta_query($query) {
			switch ($query['value_type']) {
				case 'array':
					$value = explode(',', $query['value']);
					break;
				case 'date':
					$parts = explode(',', $query['value']);
					$value = date($parts[0], strtotime($parts[1]));
					break;
				default:
					$value = $query['value'];
					break;
			}
			$meta_query = array (
				'key' => $query['key'],
				'value' => $value,
				'compare' => $query['compare'],
				'type' => $query['type'],
			);

		return $meta_query;
	}

	/**
	 * Get items by post settings
	 */
	function _s_get_post_items_by_settings( $settings, $post_type = 'post' ) {
		if (get_query_var('page')) {
			$paged = get_query_var('page');
		} elseif (get_query_var('paged')) {
			$paged = get_query_var('paged');
		} else {
			$paged = 1;
		}

		$meta_query = array_map("\Elementor\\_s_map_meta_query", $settings['meta_query']);
		$meta_query['relation'] = $settings['meta_query_relation'];

		// Portfolio category
		$tax_query = '';
		if ($post_type == 'portfolio' && !empty($settings['category'])) {
			$tax_query = array(
				array(
					'taxonomy' => 'portfolio_category',
					'field'    => 'slug',
					'terms'    => explode(',', $settings['category']),
				),
			);
			$settings['category'] = '';
		}
		
		$post_args = array (
			'posts_per_page' => $settings['posts_per_page'],
			'offset' => $settings['offset'],
			'category_name' => $settings['category'], 
			'post__in' => !empty(trim($settings['include'])) ? explode( ',', $settings['include']) : '', 
			'post__not_in' => explode( ',', $settings['exclude']), 
			'orderby' => $settings['orderby'],
			'order' => $settings['order'],
			'meta_query' => $meta_query,
			'post_type' => $post_type,
			'paged' => $paged,
			'tax_query' => $tax_query
		);
		$posts = new \WP_Query($post_args);

		return $posts;
	}

	/**
	 * Add theme category
	 */
	function _s_elementor_init() {
    Plugin::instance()->elements_manager->add_category(
      '_s-widgets',
      [
          'title'  => '_S_ Widgets',
          'icon' => 'font'
      ],
      1
    );
	}
	add_action('elementor/init','Elementor\_s_elementor_init');

	/**
	 * Add widgets
	 */
	function _s_add_widgets() {
		require_once get_template_directory() . '/inc/widgets/custom-map.php';
		require_once get_template_directory() . '/inc/widgets/buttons.php';
		require_once get_template_directory() . '/inc/widgets/image-gallery.php';
		require_once get_template_directory() . '/inc/widgets/carousel-base.php';
		require_once get_template_directory() . '/inc/widgets/carousel-portfolio.php';
		require_once get_template_directory() . '/inc/widgets/carousel-post.php';
		require_once get_template_directory() . '/inc/widgets/carousel-content.php';
		require_once get_template_directory() . '/inc/widgets/carousel-testimonial.php';
		require_once get_template_directory() . '/inc/widgets/grid-base.php';
		require_once get_template_directory() . '/inc/widgets/grid-image.php';
		require_once get_template_directory() . '/inc/widgets/grid-portfolio.php';
		require_once get_template_directory() . '/inc/widgets/grid-post.php';
		require_once get_template_directory() . '/inc/widgets/grid-product.php';
		require_once get_template_directory() . '/inc/widgets/grid-custom-post-types.php';
		require_once get_template_directory() . '/inc/widgets/team-member.php';
		require_once get_template_directory() . '/inc/widgets/parallax-controls.php';
		require_once get_template_directory() . '/inc/widgets/query-controls.php';
	}
	add_action('elementor/widgets/widgets_registered','Elementor\_s_add_widgets');

}
