<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('elementor/controls/controls_registered', function($el) {

    class _S_Elementor_Control_Icon extends Base_Data_Control {
        public function get_type() {
            return 'icon';
        }
        public static function get_icons() {
            $icons = [];
            // Get arrays of icons.
            $font_awesome_icons = require get_template_directory() . '/inc/vendor/font-awesome.php';
            $theme_icons = require get_template_directory() . '/inc/vendor/theme-icons.php';
            foreach ($font_awesome_icons as $icon) {
                $icons["fa {$icon}"] = str_replace('fa-', '', $icon);
            }
            foreach ($theme_icons as $icon) {
                $icons[$icon] = $icon;
            }
            return $icons;
        }
        protected function get_default_settings() {
            return [
                'options' => self::get_icons(),
            ];
        }
        public function content_template() {
            $control_uid = $this->get_control_uid();
            ?>
            <div class="elementor-control-field">
                <label for="<?php echo $control_uid; ?>" class="elementor-control-title">{{{ data.label }}}</label>
                <div class="elementor-control-input-wrapper">
                    <select id="<?php echo $control_uid; ?>" class="elementor-control-icon" data-setting="{{ data.name }}" data-placeholder="<?php _e( 'Select Icon', '_s' ); ?>">
                        <option value=""><?php _e( 'Select Icon', '_s' ); ?></option>
                        <# _.each( data.options, function( option_title, option_value ) { #>
                        <option value="{{ option_value }}">{{{ option_title }}}</option>
                        <# } ); #>
                    </select>
                </div>
            </div>
            <# if ( data.description ) { #>
            <div class="elementor-control-field-description">{{ data.description }}</div>
            <# } #>
            <?php
        }
    }
    $el->register_control('icon', new _S_Elementor_Control_Icon);
    
});