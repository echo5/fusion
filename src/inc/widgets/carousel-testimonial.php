<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Testimonial_Carousel extends Widget_Carousel_Base {

	public function get_name() {
		return 'testimonial-carousel';
	}

	public function get_title() {
		return esc_html__( 'Testimonial Carousel', '_s' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_testimonial_carousel',
			[
				'label' => esc_html__( 'Carousel', '_s' ),
			]
		);

		$this->add_control(
			'slides',
			[
				'label' => esc_html__( 'Slides', '_s' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'slide_content' => esc_html__( 'Slide #1 content', '_s' ),
					],
					[
						'slide_content' => esc_html__( 'Slide #2 content', '_s' ),
					],
				],
				'fields' => [
					[
						'name' => 'user_name',
						'label' => esc_html__( 'User Name', '_s' ),
						'type' => Controls_Manager::TEXT,
						'default' => esc_html__( 'John Doe', '_s' ),
						'show_label' => false,
					],
					[
						'name' => 'user_title',
						'label' => esc_html__( 'User Title', '_s' ),
						'type' => Controls_Manager::TEXT,
						'default' => esc_html__( 'Founder & Ceo', '_s' ),
						'show_label' => false,
					],
					[
						'name' => 'user_image',
						'label' => esc_html__( 'User Image', '_s' ),
						'type' => Controls_Manager::MEDIA,
						'default' => [
							'url' => Utils::get_placeholder_image_src(),
						],
						'show_label' => false,
					],
					[
						'name' => 'content',
						'label' => esc_html__( 'Content', '_s' ),
						'type' => Controls_Manager::WYSIWYG,
						'default' => esc_html__( 'Slide Content', '_s' ),
						'show_label' => false,
					],
				],
				'title_field' => '{{{ user_name }}}',
			]
		);

		$this->add_control(
			'alignment',
			[
				'label' => esc_html__( 'Alignment', '_s' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'center' => esc_html__( 'Center', '_s' ),
					'left' => esc_html__( 'Left', '_s' ),
					'right' => esc_html__( 'Right', '_s' ),
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label' => esc_html__( 'View', '_s' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->carousel_controls();

	}

	protected function render() {

		$settings = $this->get_settings();
		$slides = [];
		$content_slides = $settings['slides'];

		if (empty($content_slides))
			return;

		foreach ($content_slides as $slide) {
			$params = [
				'user_name' => $slide['user_name'],
				'user_title' => $slide['user_title'],
				'user_image' => $slide['user_image'],
				'content' => $slide['content'],
				'alignment' => $settings['alignment']
			];
			$slides[] = \_s_load_template_part( 'content-testimonial', $params );
		}

		$this->render_carousel($slides, $settings);

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Testimonial_Carousel() );