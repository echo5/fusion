<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Content_Carousel extends Widget_Carousel_Base {

	public function get_name() {
		return 'content-carousel';
	}

	public function get_title() {
		return esc_html__( 'Content Carousel', '_s' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_content_carousel',
			[
				'label' => esc_html__( 'Carousel', '_s' ),
			]
		);

		$this->add_control(
			'slides',
			[
				'label' => esc_html__( 'Slides', '_s' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'slide_content' => esc_html__( 'Slide #1 content', '_s' ),
					],
					[
						'slide_content' => esc_html__( 'Slide #2 content', '_s' ),
					],
				],
				'fields' => [
					[
						'name' => 'slide_content',
						'label' => esc_html__( 'Content', '_s' ),
						'type' => Controls_Manager::WYSIWYG,
						'default' => esc_html__( 'Slide Content', '_s' ),
						'show_label' => false,
					],
				],
				// 'title_field' => '{{{ tab_title }}}',
			]
		);

		$this->carousel_controls();

	}

	protected function render() {

		$settings = $this->get_settings();
		$slides = [];
		$content_slides = $settings['slides'];

		if (empty($content_slides))
			return;

		foreach ($content_slides as $slide) {
			$slides[] = $slide['slide_content'];
		}

		$this->render_carousel($slides, $settings);

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Content_Carousel() );