<?php

LuxeOption::add_section( 'header_dark', array(
    'title'          => esc_attr__( 'Dark Background Header', '_s' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'header'
) );

LuxeOption::add_field( 'luxe_options', array(
    'settings' => 'logo_dark',
    'label'    => esc_attr__( 'Dark Background Logo', '_s' ),
    'section'  => 'header_dark',
    'type'     => 'image',
    'priority' => 10,
    'default'  => '',
    // 'transport'   => 'postMessage',
    // 'js_vars'     => array(
    //     array(
    //         'element'  => 'header .navbar-brand',
    //         'function' => 'customize_preview_js',
    //     ),
    // ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'header_dark_bg_color',
    'label'       => esc_attr__( 'Header Background Color', '_s' ),
    'description' => esc_attr__( 'Set the color of your header\'s background.', '_s' ),
    'help'        => esc_attr__( 'This is a tooltip', '_s' ),
    'section'     => 'header_dark',
    'default'     => '#fff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-dark-active .navbar, .header-dark-active .navbar-inner',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.header-dark-active .navbar, .header-dark-active .navbar-inner',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'header_dark_typography_color',
    'label'       => esc_attr__( 'Navigation Font Color', '_s' ),
    'description' => esc_attr__( 'Set the color of your dark header font.', '_s' ),
    'section'     => 'header_dark',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-dark-active .navbar, .header-dark-active .navbar a, .header-dark-active .navbar i.icon',
            'property' => 'color',
        ),
        array(
            'element'  => '.header-dark-active .nav-btn .nav-icon span',
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.header-dark-active .navbar, .header-dark-active .navbar a, .header-dark-active .navbar i.icon',
            'function' => 'css',
            'property' => 'color',
        ),
        array(
            'element'  => '.header-dark-active .nav-btn .nav-icon span',
            'function' => 'css',
            'property' => 'border-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'header_dark_typography_color_hover',
    'label'       => esc_attr__( 'Navigation Font Hover Color', '_s' ),
    'description' => esc_attr__( 'Set the color of your dark header font when hovered.', '_s' ),
    'section'     => 'header_dark',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-dark-active .navbar a:hover, .header-dark-active .navbar .nav-btn:hover, .header-dark-active .nav-primary li.current-menu-item a, .nav-hover-effect-underline .nav-primary a:after, .header-dark-active a:hover i.icon',
            'property' => 'color',
        ),
        array(
            'element'  => '.header-dark-active .nav-btn:hover .nav-icon span',
            'property' => 'border-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'header_dark_border_width',
    'label'       => esc_attr__( 'Dark Header Border Width', '_s' ),
    'description' => esc_attr__( 'The border between your dark header and content.', '_s' ),
    'section'     => 'header_dark',
    'default'     => '0px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-dark-active .navbar',
            'property' => 'border-width',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.header-dark-active .navbar',
            'function' => 'css',
            'property' => 'border-width',
        ),
    ),
    'choices' => array(
        'units' => array( 'px' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'header_dark_border_color',
    'label'       => esc_attr__( 'Dark Header Border Color', '_s' ),
    'description' => esc_attr__( 'Set the color of your dark header font when hovered.', '_s' ),
    'section'     => 'header_dark',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-dark-active .navbar',
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.header-dark-active .navbar',
            'function' => 'css',
            'property' => 'border-color',
        ),
    ),
) );
