<?php

LuxeOption::add_panel( 'header', array(
    'title'          => esc_attr__( 'Header', '_s' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );
LuxeOption::add_section( 'header_base', array(
    'title'          => esc_attr__( 'Base Header', '_s' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'header'
) );

/**
 * Basic header styles
 */
$header_styles = apply_filters( 'luxe_header_styles', array('default' => 'Default'));
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'header_style',
    'label'       => esc_attr__( 'Header Style', '_s' ),
    'description' => esc_attr__( 'Pick the style and layout of your header.', '_s' ),
    'section'     => 'header_base',
    'default'     => 'none',
    'priority'    => 10,
    'choices'     => $header_styles
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'header_scheme',
    'label'       => esc_attr__( 'Default Header Scheme', '_s' ),
    'description' => esc_attr__( 'Select default header used for pages.  This can be changed on each individual page.', '_s' ),
    'section'     => 'header_base',
    'default'     => 'light',
    'priority'    => 10,
    'choices'     => array(
        'light'   => esc_attr__( 'Light Background Header', '_s' ),
        'dark' => esc_attr__( 'Dark Background Header', '_s' ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'offcanvas_header_scheme',
    'label' => esc_attr__( 'Offcanvas Header Scheme ', '_s' ),
    'description'       => esc_attr__( 'The header color scheme to use when an overlay or slide-out navigation is open.', '_s' ),
    'section'     => 'header_base',
    'default'     => 'light',
    'priority'    => 10,
    'choices'     => array(
        'light'   => esc_attr__( 'Light Background Header', '_s' ),
        'dark' => esc_attr__( 'Dark Background Header', '_s' ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'header_position',
    'label'       => esc_attr__( 'Header Position', '_s' ),
    'description' => esc_attr__( 'Choose if your header is sticky or stays at the top of the page.', '_s' ),
    'section'     => 'header_base',
    'default'     => 'absolute',
    'priority'    => 10,
    'choices'     => array(
        'absolute' => 'Top of page',
        'fixed'   => 'Fixed to top',
    ),
    'output' => array(
        array(
            'element' => '.navbar',
            'function' => 'css',
            'property' => 'position',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'nav_button',
    'label'       => esc_attr__( 'Navigation Button', '_s' ),
    'description' => esc_attr__( 'Select the style of your navigation / hamburger icon.  This is used in certain header styles and for your mobile navigation.', '_s' ),
    'section'     => 'header_base',
    'default'     => 'icon',
    'priority'    => 10,
    'choices'     => array(
        'icon'   => esc_attr__( 'Icon', '_s' ),
        'text' => esc_attr__( 'Text', '_s' ),
        'icon_and_text'      => esc_attr__( 'Icon and Text', '_s' ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'text',
    'settings'    => 'nav_button_text',
    'label'       => esc_attr__( 'Navigation Button Text', '_s' ),
    'description' => esc_attr__( 'The text used as your navigation button.', '_s' ),
    'section'     => 'header_base',
    'default'     => 'Menu',
    'priority'    => 10,
    'required'    => array(
        array(
            'setting'  => 'nav_button',
            'operator' => '!=',
            'value'    => 'icon',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'header_height',
    'label'       => esc_attr__( 'Header Height', '_s' ),
    'description' => esc_attr__( 'Control the height of your header in pixels.', '_s' ),
    'section'     => 'header_base',
    'default'     => '80',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.navbar, .navbar-inner',
            'property' => 'height',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.navbar, .navbar-inner',
            'property' => 'height',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 50,
        'max'  => 300,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'logo_padding_top',
    'label'       => esc_attr__( 'Logo Top Padding', '_s' ),
    'description' => esc_attr__( 'Control the padding in pixels above your logo.', '_s' ),
    'section'     => 'header_base',
    'default'     => '0',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.navbar-brand',
            'property' => 'padding-top',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.navbar-brand',
            'property' => 'padding-top',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 80,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'logo_padding_bottom',
    'label'       => esc_attr__( 'Logo Bottom Padding', '_s' ),
    'description' => esc_attr__( 'Control the padding in pixels below your logo.', '_s' ),
    'section'     => 'header_base',
    'default'     => '0',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.navbar-brand',
            'property' => 'padding-bottom',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.navbar-brand',
            'property' => 'padding-bottom',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 80,
        'step' => 1,
    )
) );
Kirki::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'logo_max_width',
    'label'       => esc_html__( 'Logo Maximum Width', '_s' ),
    'section'     => 'header_base',
    'default'     => '',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.navbar-brand img',
            'property' => 'max-width',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.navbar-brand img',
            'property' => 'max-width',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'header_typography',
    'label'       => esc_attr__( 'Header Typography', '_s' ),
    'description' => esc_attr__( 'Typography for other elements in your header.', '_s' ),
    'section'     => 'header_base',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Roboto',
        'font-size'      => '14',
        'font-weight'    => '400',
        'line-height'    => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => false,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.navbar',
        ),
    ),
) );

