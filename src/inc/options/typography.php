<?php

function h1_typography_mobile($value) {
    preg_match_all('/^(\d+|\d*\.\d+)(\w+)$/', $value['font-size'], $font_size);
    $new_font_size = $font_size[1][0] * .666;
    return array(
        'font-size' => $new_font_size . $font_size[2][0],
    );
}
function h1_typography_tablets($value) {
    preg_match_all('/^(\d+|\d*\.\d+)(\w+)$/', $value['font-size'], $font_size);
    $new_font_size = $font_size[1][0] * .83;
    return array(
        'font-size' => $new_font_size . $font_size[2][0],
    );
}
function h2_typography_mobile($value) {
    preg_match_all('/^(\d+|\d*\.\d+)(\w+)$/', $value['font-size'], $font_size);
    $new_font_size = $font_size[1][0] * .72;
    return array(
        'font-size' => $new_font_size . $font_size[2][0],
    );
}
function h2_typography_tablets($value) {
    preg_match_all('/^(\d+|\d*\.\d+)(\w+)$/', $value['font-size'], $font_size);
    $new_font_size = $font_size[1][0] * .889;
    return array(
        'font-size' => $new_font_size . $font_size[2][0],
    );
}
function h3_typography_mobile($value) {
    preg_match_all('/^(\d+|\d*\.\d+)(\w+)$/', $value['font-size'], $font_size);
    $new_font_size = $font_size[1][0] * .78;
    return array(
        'font-size' => $new_font_size . $font_size[2][0],
    );
}
function h3_typography_tablets($value) {
    preg_match_all('/^(\d+|\d*\.\d+)(\w+)$/', $value['font-size'], $font_size);
    $new_font_size = $font_size[1][0] * .857;
    return array(
        'font-size' => $new_font_size . $font_size[2][0],
    );
}


LuxeOption::add_section( 'typography', array(
    'title'          => esc_attr__( 'Typography', '_s' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * Body Typography
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'repeater',
    'label'       => esc_attr__( 'Custom Fonts', '_s' ),
    'description' => esc_attr__( 'Add your font name and font file links here.  After saving they will appear in your typography options.', '_s' ),
    'help'        => esc_attr__( 'If you\'re using a service like TypeKit you only need to add the name here.', '_s' ),
    'section'     => 'typography',
    'priority'    => 10,
    'settings'    => 'custom_fonts',
    'default'     => array(
        // array(
        //     'link_text' => esc_attr__( 'Kirki Site', '_s' ),
        //     'link_url'  => 'https://kirki.org',
        // ),
        // array(
        //     'link_text' => esc_attr__( 'Kirki Repository', '_s' ),
        //     'link_url'  => 'https://github.com/aristath/kirki',
        // ),
    ),
    'fields' => array(
        'font_name' => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Font Name', '_s' ),
            'default'     => '',
        ),
        // 'link_url' => array(
        //     'type'        => 'text',
        //     'label'       => esc_attr__( 'Link URL', '_s' ),
        //     'description' => esc_attr__( 'This will be the link URL', '_s' ),
        //     'default'     => '',
        // ),
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'typography',
    'label'       => esc_attr__( 'Body Text Typography', '_s' ),
    'description' => esc_attr__( 'The font used for text across your site.  This generally applies to all p tags, span tags, and any text not included in header tags.', '_s' ),
    'section'     => 'typography',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Muli',
        'font-size'      => '1rem',
        'font-weight'    => '400',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#9192a4',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => true,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => 'body',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'link_color',
    'label'       => esc_attr__( 'Link Color', '_s' ),
    'description' => esc_attr__( 'Set the color of all links in your page content.', '_s' ),
    'section'     => 'typography',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'a',
            'property' => 'color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.main a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'link_color_hover',
    'label'       => esc_attr__( 'Link Hover Color', '_s' ),
    'description' => esc_attr__( 'Set the color of all links in your page content when hovered.', '_s' ),
    'section'     => 'typography',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'a:hover',
            'property' => 'color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'h1_typography',
    'label'       => esc_attr__( 'H1 Typography', '_s' ),
    'section'     => 'typography',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Muli',
        'font-size'      => '3em',
        'font-weight'    => '800',
        'line-height'    => '1.2',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#000000',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => true,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => 'h1, .h1',
        ),
        array(
            'element' => 'h1, .h1',
            'media_query' => '@media (max-width: 991px)',
            'sanitize_callback' => 'h1_typography_tablets',
        ),
        array(
            'element' => 'h1, .h1',
            'media_query' => '@media (max-width: 767px)',
            'sanitize_callback' => 'h1_typography_mobile',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'h2_typography',
    'label'       => esc_attr__( 'H2 Typography', '_s' ),
    'section'     => 'typography',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Muli',
        'font-size'      => '32',
        'font-weight'    => '800',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#000000',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => true,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    // 'choices'     => Kirki_Fonts::get_font_choices(),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => 'h2, .h2, .elementor-drop-cap-letter',
        ),
        array(
            'element' => 'h2, .h2, .elementor-drop-cap-letter',
            'media_query' => '@media (max-width: 991px)',
            'sanitize_callback' => 'h2_typography_tablets',
        ),
        array(
            'element' => 'h2, .h2, .elementor-drop-cap-letter',
            'media_query' => '@media (max-width: 767px)',
            'sanitize_callback' => 'h2_typography_mobile',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'h3_typography',
    'label'       => esc_attr__( 'H3 Typography', '_s' ),
    'section'     => 'typography',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Muli',
        'font-size'      => '22',
        'font-weight'    => '800',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#000000',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => true,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    // 'choices'     => Kirki_Fonts::get_font_choices(),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => 'h3, .h3',
        ),
        array(
            'element' => 'h3, .h3',
            'media_query' => '@media (max-width: 991px)',
            'sanitize_callback' => 'h3_typography_tablets',
        ),
        array(
            'element' => 'h3, .h3',
            'media_query' => '@media (max-width: 767px)',
            'sanitize_callback' => 'h3_typography_mobile',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'h4_typography',
    'label'       => esc_attr__( 'H4 Typography', '_s' ),
    'section'     => 'typography',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Muli',
        'font-size'      => '18px',
        'font-weight'    => '800',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#000000',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => true,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    // 'choices'     => Kirki_Fonts::get_font_choices(),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => 'h4, .h4, .comment-reply-title',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'h5_typography',
    'label'       => esc_attr__( 'H5 Typography', '_s' ),
    'section'     => 'typography',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Muli',
        'font-size'      => '16',
        'font-weight'    => '800',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#000000',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => true,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    // 'choices'     => Kirki_Fonts::get_font_choices(),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => 'h5, .h5',
        ),
    ),
) );
