<?php

LuxeOption::add_section( 'body', array(
    'title'          => esc_attr__( 'Body', '_s' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * Base
 */

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'container_width',
    'label'       => esc_attr__( 'Maximum Container Width', '_s' ),
    'description' => esc_attr__( 'Controls how wide your content is on larger screens.', '_s' ),
    'help'        => esc_attr__( 'This does not apply to full browser width sections.', '_s' ),
    'section'     => 'body',
    'default'     => '1170px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.site-content.container, .elementor-section.elementor-section-boxed > .elementor-container',
            'property' => 'max-width',
            'media_query' => '@media (min-width: 1200px)'
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.container',
            'property' => 'max-width',
            'function' => 'css',
        ),
    ),
    'choices' => array(
        'units' => array( 'px', '%' )
    ),
) );


LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'particle_background',
    'label'       => esc_attr__( 'Global Particle Background', '_s' ),
    'description' => esc_attr__( 'Bouncing particles loaded in the background.  If enabled, global particle settings will override the individual page particle settings.', '_s' ),
    'section'     => 'body',
    'default'     => false,
    'priority'    => 10,
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'text',
    'settings'    => 'particle_colors',
    'label'       => esc_attr__( 'Particle Colors', '_s' ),
    'description' => esc_attr__( 'Colors separated by commas without spaces.  You can use color names or hex values here.', '_s' ),
    'section'     => 'body',
    'default'     => 'red,#f5f5f5',
    'priority'    => 10,
    'required'    => array(
        array(
            'setting'  => 'particle_background',
            'operator' => '==',
            'value'    => true,
        ),
    ),
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'number',
    'settings'    => 'particle_count',
    'label'       => esc_attr__( 'Particle Count', '_s' ),
    'section'     => 'body',
    'default'     => 20,
    'priority'    => 10,
    'choices'     => array(
        'min'  => 1,
        'max'  => 200,
        'step' => 1,
    ),
    'required'    => array(
        array(
            'setting'  => 'particle_background',
            'operator' => '==',
            'value'    => true,
        ),
    ),
) );