<?php

LuxeOption::add_panel( 'buttons', array(
    'title'          => esc_attr__( 'Buttons', '_s' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

LuxeOption::add_section( 'buttons', array(
    'title'          => esc_attr__( 'Default Buttons', '_s' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'buttons',
) );


function _s_buttons() {
    $buttons = array(
        '.button',
        '.btn.btn-default',
        '.btn.btn-primary',
        'input[type=submit]',
        '.elementor-widget-button a.elementor-button',
        '.woocommerce input.button',
        '.woocommerce #respond input#submit.alt',
        '.woocommerce a.button.alt',
        '.woocommerce button.button.alt',
        '.woocommerce input.button.alt',
        '.woocommerce a.button',
        '.woocommerce button.button.alt:disabled[disabled]',
        '.dummy-keep-at-end-btn',
    );
    return apply_filters('luxe_buttons', $buttons);
}
$buttons = _s_buttons();


/**
 * Buttons
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'button_typography',
    'label'       => esc_attr__( 'Button Typography', '_s' ),
    'section'     => 'buttons',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Roboto',
        'font-size'      => '16px',
        'font-weight'    => '400',
        'line-height'    => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#ffffff',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => false,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    // 'choices'     => Kirki_Fonts::get_font_choices(),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => implode(', ', $buttons),
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'button_text_color_hover',
    'label'       => esc_attr__( 'Button Hover Text Color', '_s' ),
    'section'     => 'buttons',
    'default'     => '#ffffff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'color',
            'suffix' => ' !important'
        ),
    ),
    'transport'   => 'postMessage',
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_bg_color',
    'label'       => esc_attr__( 'Button Background Color', '_s' ),
    'section'     => 'buttons',
    'default'     => '#191919',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.btn.btn-default',
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_bg_color_hover',
    'label'       => esc_attr__( 'Button Hover Background Color', '_s' ),
    'section'     => 'buttons',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'button_padding_vertical',
    'label'       => esc_attr__( 'Button Height', '_s' ),
    'description' => esc_attr__( 'Control the padding in pixels above and below your button text.', '_s' ),
    'section'     => 'buttons',
    'default'     => '15',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-top',
            'units'    => 'px',
            'suffix'    => ' !important',
        ),
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-bottom',
            'units'    => 'px',
            'suffix'    => ' !important',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-top',
            'units'    => 'px',
            'function' => 'css',
            'suffix'    => ' !important',
        ),
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-bottom',
            'units'    => 'px',
            'function' => 'css',
            'suffix'    => ' !important',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 60,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'button_padding_horizontal',
    'label'       => esc_attr__( 'Button Width', '_s' ),
    'description' => esc_attr__( 'Control the padding in pixels to the left and right of your button text.', '_s' ),
    'section'     => 'buttons',
    'default'     => '15',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-left',
            'units'    => 'px',
        ),
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-right',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-left',
            'units'    => 'px',
            'function' => 'css',
            'suffix'    => ' !important',
        ),
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-right',
            'units'    => 'px',
            'function' => 'css',
            'suffix'    => ' !important',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 80,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'button_radius',
    'label'       => esc_attr__( 'Button Shape', '_s' ),
    'section'     => 'buttons',
    'default'     => '0px',
    'priority'    => 10,
    'exclude'     => 'underline',
    'choices'     => array(
        ''      => esc_attr__( 'Select Button Shape', '_s' ),
        '0px'   => esc_attr__( 'Square', '_s' ),
        '5px' => esc_attr__( 'Rounded', '_s' ),
        '2em'  => esc_attr__( 'Round', '_s' ),
        'underline'  => esc_attr__( 'Underline', '_s' ),
    ),
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-radius',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-radius',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'button_border_width',
    'label'       => esc_attr__( 'Button Border Width', '_s' ),
    'description' => esc_attr__( 'Controls how wide your content is on larger screens.', '_s' ),
    'help'        => esc_attr__( 'This does not apply to full browser width sections.', '_s' ),
    'section'     => 'buttons',
    'default'     => '0px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons) . ', ' . implode(':hover, ', $buttons),
            'property' => 'border-width',
            'suffix' => '; border-style: solid'
        ),
    ),
    'choices' => array(
        'units' => array( 'px' )
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-width',
            'suffix' => '; border-style: solid'
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_border_color',
    'label'       => esc_attr__( 'Button Border Color', '_s' ),
    'section'     => 'buttons',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_border_color_hover',
    'label'       => esc_attr__( 'Button Hover Border Color', '_s' ),
    'section'     => 'buttons',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'border-color',
        ),
    ),
) );


LuxeOption::add_section( 'buttons_primary', array(
    'title'          => esc_attr__( 'Primary Buttons', '_s' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'buttons',
) );

$buttons = array();
$buttons[] = '.button';
$buttons[] = 'input[type=submit]';
$buttons[] = '.comment-form input[type=submit]';
$buttons[] = '.btn.btn-primary';
$buttons[] = '.btn-primary.elementor-widget-button a.elementor-button';
$buttons[] = '.woocommerce a.button';
$buttons[] = '.woocommerce input.button';
$buttons[] = '.woocommerce button.button.alt:disabled[disabled]';
$buttons[] = '.dummy-keep-at-end-btn';

/**
 * Primary Buttons
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_primary_bg_color',
    'label'       => esc_attr__( 'Button Background Color', '_s' ),
    'section'     => 'buttons_primary',
    'default'     => '#f1f1f1',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_primary_bg_color_hover',
    'label'       => esc_attr__( 'Button Hover Background Color', '_s' ),
    'section'     => 'buttons_primary',
    'default'     => '#f0f0f3',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'button_primary_text_color',
    'label'       => esc_attr__( 'Button Text Color', '_s' ),
    'section'     => 'buttons_primary',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'button_primary_text_color_hover',
    'label'       => esc_attr__( 'Button Hover Text Color', '_s' ),
    'section'     => 'buttons_primary',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'color',
            'suffix' => ' !important'
        ),
    ),
    'transport'   => 'postMessage',
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_primary_border_color',
    'label'       => esc_attr__( 'Button Border Color', '_s' ),
    'section'     => 'buttons_primary',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_primary_border_color_hover',
    'label'       => esc_attr__( 'Button Hover Border Color', '_s' ),
    'section'     => 'buttons_primary',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'border-color',
        ),
    ),
) );

function _s_button_inline_styles() {
    $button_style = get_theme_mod( 'button_radius', '' );
    if ($button_style == 'underline') {
        $custom_css = implode(', ', _s_buttons()) . " {
            border: 0 !important;
            background-color: transparent !important;
        }";
        $custom_css .= implode(':after, ', _s_buttons()) . " {
            content: '';
            display: block;
            height: 2px;
            width: 30%;
            background-color: " . get_theme_mod( 'button_border_color', 'black' ) . ";
            margin-top: 1rem;
            transition: width .3s ease-in-out;
        }";
        $custom_css .= implode(':hover:after, ', _s_buttons()) . " {
            width: 70%;
        }";
        wp_add_inline_style( '_s-main', $custom_css );
    }
}
add_action( 'wp_enqueue_scripts', '_s_button_inline_styles', 101 );

