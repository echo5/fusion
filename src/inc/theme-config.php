<?php

/**
 * Bootstrap classes
 */
require_once get_template_directory() . '/inc/vendor/wp-bootstrap-navwalker.php';
require_once get_template_directory() . '/inc/vendor/bootstrap-comment-walker.php';

/**
 * Enqueue admin scripts and styles.
 */
function _s_admin_scripts() {
  wp_enqueue_script( '_s-admin', get_template_directory_uri() . '/assets/js/admin.js', array( 'wp-color-picker' ), '1.2.2', true );
}
add_action( 'admin_enqueue_scripts', '_s_admin_scripts' );

/**
 * Image sizes
 */
add_image_size( '_s-wide', 470, 300, true ); // 1.56
add_image_size( '_s-medium', 560, 450, true ); // 1.24
add_image_size( '_s-tall', 470, 600, true ); // 0.78
add_image_size( '_s-taller', 470, 700, true ); // 0.67
function _s_add_image_sizes( $sizes ) {
  return array_merge( $sizes, array(
    '_s-wide' => esc_html__( 'Wide', '_s' ),
    '_s-medium' => esc_html__( 'Medium', '_s' ),
    '_s-tall' => esc_html__( 'Tall', '_s' ),
    '_s-taller' => esc_html__( 'Taller', '_s' ),
  ) );
}
add_filter( 'image_size_names_choose', '_s_add_image_sizes' );

/**
 * Set default date format
 *
 * @return string
 */
if (!function_exists('_s_default_date_format')) {
  function _s_default_date_format() {
    return 'j F, Y';
  }
}

/**
 * Set header styles
 *
 * @return array
 */
function _s_header_styles() {
  return array(
    'default' => 'Default',
    'nav-left' => 'Navigation Left',
    'floating' => 'Floating',
    'overlay' => 'Overlay',
    'side-collapsed' => 'Side Collapsed',
    'side' => 'Side Navigation',
    'slide-down' => 'Slide Down',
  );
}
add_filter( 'luxe_header_styles', '_s_header_styles' );

/**
 * Set portfolio styles
 *
 * @return array
 */
function _s_portfolio_single_styles() {
  return array(
    'carousel-right' => 'Carousel Right',
    'meta-right' => 'Meta Right',
    'images-bottom' => 'Images Bottom',
    'images-right' => 'Images Right',
    'slider-right' => 'Slider Right',
  );
}
add_filter( 'luxe_portfolio_single_styles', '_s_portfolio_single_styles' );

/**
 * Set portfolio styles
 *
 * @return array
 */
function _s_portfolio_grid_styles() {
  return array(
    'default' => 'Default',
    'gradient-bottom' => 'Gradient Bottom',
    'label' => 'Label',
    'layered' => 'Layered',
    'overlay' => 'Overlay',
    'overlay-left' => 'Overlay Left',
    'overlay-excerpt' => 'Overlay with Excerpt',
    'overlay-inset' => 'Overlay Inset',
    'clip-text' => 'Clip Text',
  );
}
add_filter( 'luxe_portfolio_grid_styles', '_s_portfolio_grid_styles' );

/**
 * Set post styles
 *
 * @return array
 */
function _s_post_single_styles() {
  return array(
    'featured-top' => 'Featured Top',
    'card' => 'Card',
    'sidebar' => 'Sidebar',
    'default' => 'Default',
    'featured-left' => 'Featured Left',
  );
}
add_filter( 'luxe_post_single_styles', '_s_post_single_styles' );

/**
 * Set post styles
 *
 * @return array
 */
function _s_post_grid_styles() {
  return array(
    'default' => 'Default',
    'no-image' => 'No Image',
    'image-left' => 'Image Left',
    'card' => 'Card Excerpt',
    'excerpt' => 'With Excerpt',
    'gradient-bottom' => 'Gradient Bottom',
    'reverse-overlay' => 'Reverse Overlay',
  );
}
add_filter( 'luxe_post_grid_styles', '_s_post_grid_styles' );

/**
 * Strip first gallery to use as featured content
 *
 * @return string
 */
function _s_strip_first_gallery( $output, $attr ) {
    if ( is_singular('portfolio') ) {
    	remove_filter( current_filter(), __FUNCTION__ );
    	return '<!-- gallery removed -->';
    }
}

add_filter( 'post_gallery', '_s_strip_first_gallery', 10, 2 );

/**
 * Add meta box to portfolio
 */
function _s_add_portfolio_meta_box()
{
    $screens = ['portfolio'];
    foreach ($screens as $screen) {
        add_meta_box(
            'portfolio_details',
            'Portfolio Details',
            '_s_portfolio_meta_box_html',
            $screen,
            'side',
            'core'
        );
    }
}
add_action('add_meta_boxes', '_s_add_portfolio_meta_box');

/**
 * Portfolio meta box html
 */
function _s_portfolio_meta_box_html($post)
{
  wp_enqueue_style( 'wp-color-picker');
  wp_enqueue_script( 'wp-color-picker');
	$post_meta = get_post_meta($post->ID);
  ?>
  <label for="made_by">Made By</label>
  <input type="text" name="made_by" id="made_by" class="postbox" value="<?php echo (isset($post_meta['_made_by'])? $post_meta['_made_by'][0] : ''); ?>">
  <label for="client">Client</label>
  <input type="text" name="client" id="client" class="postbox" value="<?php echo (isset($post_meta['_client'])? $post_meta['_client'][0] : ''); ?>">
  <label for="date">Date</label>
  <input type="text" name="date" id="date" class="postbox" value="<?php echo (isset($post_meta['_date'])? $post_meta['_date'][0] : ''); ?>">
  <label for="role">Role</label>
  <input type="text" name="role" id="role" class="postbox" value="<?php echo (isset($post_meta['_role'])? $post_meta['_role'][0] : ''); ?>">
  <label for="role">Accent Color</label>
  <input type="text" name="color" id="color" class="postbox color-picker" data-alpha="true" value="<?php echo (isset($post_meta['_color'])? $post_meta['_color'][0] : ''); ?>">
  <?php
}

/**
 * Save portfolio meta info
 */
function _s_save_portfolio_post_meta($post_id)
{
	global $post;
  if ($post && $post->post_type == 'portfolio') {
    if (isset($_POST['made_by'])) {
      update_post_meta( $post_id, '_made_by', esc_html($_POST['made_by']) );
    }
    if (isset($_POST['client'])) {
      update_post_meta( $post_id, '_client', esc_html($_POST['client']) );
    }
    if (isset($_POST['date'])) {
      update_post_meta( $post_id, '_date', esc_html($_POST['date']) );
    }
    if (isset($_POST['role'])) {
      update_post_meta( $post_id, '_role', esc_html($_POST['role']) );
    }
    if (isset($_POST['color'])) {
      update_post_meta( $post_id, '_color', esc_html($_POST['color']) );
    }
  }
}
add_action('save_post', '_s_save_portfolio_post_meta');

/**
 * Register widget area.
 */
function _s_widgets_init() {
  register_sidebar( array(
    'name'          => esc_html__( 'Sidebar', '_s' ),
    'id'            => 'sidebar-1',
    'description'   => esc_html__( 'Add widgets here.', '_s' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Header 1', '_s' ),
    'id'            => 'header-1',
    'description'   => esc_html__( 'Add widgets here.', '_s' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Header 2', '_s' ),
    'id'            => 'header-2',
    'description'   => esc_html__( 'Add widgets here.', '_s' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  if ( _s_is_elementor_active() ) {
    register_sidebar( array(
      'name'          => esc_html__( 'Header Override', '_s' ),
      'id'            => 'header-override',
      'description'   => esc_html__( 'This widget area will replace your theme header.  Add elementor header templates or AE templates here.', '_s' ),
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>',
    ) );
  }

  if ( _s_is_elementor_active() ) {
    register_sidebar( array(
      'name'          => esc_html__( 'Footer 1', '_s' ),
      'id'            => 'footer-1',
      'description'   => esc_html__( 'Add widgets here.', '_s' ),
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>',
    ) );
  }

  register_sidebar( array(
    'name'          => esc_html__( 'Fixed Corner Left', '_s' ),
    'id'            => 'fixed-corner-left',
    'description'   => esc_html__( 'Fixed corner widget on bottom left of screen at all times.', '_s' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Fixed Corner Right', '_s' ),
    'id'            => 'fixed-corner-right',
    'description'   => esc_html__( 'Fixed corner widget on bottom right of screen at all times.', '_s' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

}
add_action( 'widgets_init', '_s_widgets_init' );

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function _s_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', '_s_custom_excerpt_length', 999 );

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function _s_excerpt_more_ellipsis( $more ) {
    return '...';
}
add_filter( 'excerpt_more', '_s_excerpt_more_ellipsis' );

/**
 * Theme options
 */
require get_template_directory() . '/inc/options.php';

/**
 * Grid styles and classes
 */
function _s_grid_styles() {
  return [
    'masonry' => [
      [
        'classes' =>'h-470',
        'image_size' => '_s-tall'
      ],
      [
        'classes' =>'h-350',
        'image_size' => '_s-wide'
      ],
      [
        'classes' =>'h-670',
        'image_size' => '_s-taller'
      ],
      [
        'classes' =>'h-470',
        'image_size' => '_s-tall'
      ],
      [
        'classes' =>'h-470',
        'image_size' => '_s-tall'
      ],
      [
        'classes' =>'h-600',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'h-350',
        'image_size' => '_s-wide'
      ],
      [
        'classes' =>'h-670',
        'image_size' => '_s-taller'
      ],
    ],
    'wide-tall' => [
      [
        'classes' =>'col-md-4 h-250',
        'image_size' => '_s-wide'
      ],
      [
        'classes' =>'col-md-4 h-450',
        'image_size' => '_s-tall'
      ],
      [
        'classes' =>'col-md-4 h-350',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-450',
        'image_size' => '_s-wide'
      ],
      [
        'classes' =>'col-md-4 h-350',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-250',
        'image_size' => '_s-wide'
      ],
    ],
    'scattered-center' => [
      [
        'classes' =>'col-md-3 offset-md-2 h-250',
        'image_size' => '_s-wide'
      ],
      [
        'classes' =>'col-md-4 offset-md-right-2 h-450',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 offset-md-1 h-450',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-2 h-250',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-450',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 offset-md-3 h-450',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-2 offset-md-right-2 h-250',
        'image_size' => '_s-medium'
      ],
    ],
    '2-column-alternating' => [
      [
        'classes' =>'col-md-6 h-670',
        'image_size' => '_s-tall'
      ],
      [
        'classes' =>'col-md-6 h-350',
        'image_size' => '_s-wide'
      ],
      [
        'classes' =>'col-md-6 h-670',
        'image_size' => '_s-tall'
      ],
      [
        'classes' =>'col-md-6 h-350',
        'image_size' => '_s-wide'
      ],
    ],
    '2-column-alternating-spaced' => [
      [
        'classes' =>'col-md-5 h-670',
        'image_size' => '_s-wide'
      ],
      [
        'classes' =>'col-md-5 offset-md-2 h-350 mt-160 mb-160',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-5 h-350 mt-160 mb-160',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-5 offset-md-2 h-670',
        'image_size' => '_s-medium'
      ],
    ],
    'single-focal-point' => [
      [
        'classes' =>'col-md-8 h-600',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
      // Second set
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-8 h-600',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => '_s-medium'
      ],
    ],
    'scattered' => [
      [
        'classes' =>'col-md-4 mb-5 h-250',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-7 mb-5 offset-md-1 h-670',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 mb-5 h-600',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-7 mb-5 offset-md-1 h-470',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 mb-5 h-450',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-3 offset-md-1 h-250',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 h-600',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-7 mb-3 h-670',
        'image_size' => '_s-medium'
      ],
      [
        'classes' =>'col-md-4 offset-md-1 mb-5 h-300',
        'image_size' => '_s-medium'
      ],
    ],
  ];
}
add_filter( 'luxe_grid_styles', '_s_grid_styles' );

/**
 * Grid style options for dropdown
 */
function _s_grid_style_options($default_options) {
  $theme_grid_options = array();
  $grid_styles = _s_grid_styles();
  foreach ($grid_styles as $style => $settings) {
    $theme_grid_options[$style] = ucfirst(str_replace('-', ' ', $style));
  }
  return array_merge($default_options, $theme_grid_options);
}
add_filter( 'luxe_grid_style_options', '_s_grid_style_options' );


/**
 * Barba ajax
 */
function _s_ajax_scripts() {
  wp_enqueue_style( 'elementor-frontend' );
  wp_enqueue_script( 'wc-single-product' );
  wp_enqueue_style( 'font-awesome' );
  wp_enqueue_script( 'jquery-slick' );
}
add_action( 'wp_enqueue_scripts', '_s_ajax_scripts', 10 );

function _s_ajax_vars() {
  $script_variables = array(
    'ajax' => get_theme_mod( 'ajax', false ),
    'ajaxTransition' => get_theme_mod( 'ajax_transition', 'FadeTransition' ),
    'googleMapsApiKey' => get_theme_mod( 'google_maps_api_key', '' ),
  );
  wp_localize_script( '_s-main', 'themeConfig', $script_variables );
}
add_action( 'wp_enqueue_scripts', '_s_ajax_vars', 100 );

/** 
 * Comment vars
 */
function _s_comment_vars() {
  $script_variables = array(
    'processing' => esc_html__( 'Processing...', '_s' ),
    'blank' => esc_html__( 'You might have left one of the fields blank, or be posting too quickly.', '_s' ),
    'success' => esc_html__( 'Thanks for your comment. We appreciate your response.', '_s' ),
    'error' => esc_html__( 'Please wait a while before posting your next comment.', '_s' ),
    'required' => esc_html__( 'Please fill in all required fields.', '_s' ),
  );
  wp_localize_script( '_s-main', 'comments', $script_variables );
}
add_action( 'wp_enqueue_scripts', '_s_comment_vars', 100 );

/**
 * Add Woocommerce support
 */
function _s_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', '_s_woocommerce_support' );

/**
 * Ajax comments
 */
function _s_ajax_comments( $comment_ID, $comment_status ){
    if( ! empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ) {
        switch( $comment_status ) {
            case '0':
                wp_notify_moderator( $comment_ID );
            case '1':
                echo "success";
                $commentdata = get_comment( $comment_ID, ARRAY_A );
                $post = get_post( $commentdata['comment_post_ID'] );
                wp_notify_postauthor( $comment_ID );
                break;
            default:
                echo "error";
        }
        exit;
    }
}
add_action( 'comment_post', '_s_ajax_comments', 10, 2 );

/**
 * Animations array
 * @return array
 */
function _s_animations() {
    return array (
        'callout.bounce' => 'callout.bounce',
        'callout.shake' => 'callout.shake',
        'callout.flash' => 'callout.flash',
        'callout.pulse' => 'callout.pulse',
        'callout.swing' => 'callout.swing',
        'callout.tada' => 'callout.tada',
        'transition.moveRight' => 'transition.moveRight',
        'transition.fadeIn' => 'transition.fadeIn',
        'transition.fadeOut' => 'transition.fadeOut',
        'transition.flipXIn' => 'transition.flipXIn',
        'transition.flipXOut' => 'transition.flipXOut',
        'transition.flipYIn' => 'transition.flipYIn',
        'transition.flipYOut' => 'transition.flipYOut',
        'transition.flipBounceXIn' => 'transition.flipBounceXIn',
        'transition.flipBounceXOut' => 'transition.flipBounceXOut',
        'transition.flipBounceYIn' => 'transition.flipBounceYIn',
        'transition.flipBounceYOut' => 'transition.flipBounceYOut',
        'transition.swoopIn' => 'transition.swoopIn',
        'transition.swoopOut' => 'transition.swoopOut',
        'transition.whirlIn' => 'transition.whirlIn',
        'transition.whirlOut' => 'transition.whirlOut',
        'transition.shrinkIn' => 'transition.shrinkIn',
        'transition.shrinkOut' => 'transition.shrinkOut',
        'transition.expandIn' => 'transition.expandIn',
        'transition.expandOut' => 'transition.expandOut',
        'transition.bounceIn' => 'transition.bounceIn',
        'transition.bounceOut' => 'transition.bounceOut',
        'transition.bounceUpIn' => 'transition.bounceUpIn',
        'transition.bounceUpOut' => 'transition.bounceUpOut',
        'transition.bounceDownIn' => 'transition.bounceDownIn',
        'transition.bounceDownOut' => 'transition.bounceDownOut',
        'transition.bounceLeftIn' => 'transition.bounceLeftIn',
        'transition.bounceLeftOut' => 'transition.bounceLeftOut',
        'transition.bounceRightIn' => 'transition.bounceRightIn',
        'transition.bounceRightOut' => 'transition.bounceRightOut',
        'transition.slideUpIn' => 'transition.slideUpIn',
        'transition.slideUpOut' => 'transition.slideUpOut',
        'transition.slideDownIn' => 'transition.slideDownIn',
        'transition.slideDownOut' => 'transition.slideDownOut',
        'transition.slideLeftIn' => 'transition.slideLeftIn',
        'transition.slideLeftOut' => 'transition.slideLeftOut',
        'transition.slideRightIn' => 'transition.slideRightIn',
        'transition.slideRightOut' => 'transition.slideRightOut',
        'transition.slideUpBigIn' => 'transition.slideUpBigIn',
        'transition.slideUpBigOut' => 'transition.slideUpBigOut',
        'transition.slideDownBigIn' => 'transition.slideDownBigIn',
        'transition.slideDownBigOut' => 'transition.slideDownBigOut',
        'transition.slideLeftBigIn' => 'transition.slideLeftBigIn',
        'transition.slideLeftBigOut' => 'transition.slideLeftBigOut',
        'transition.slideRightBigIn' => 'transition.slideRightBigIn',
        'transition.slideRightBigOut' => 'transition.slideRightBigOut',
        'transition.perspectiveUpIn' => 'transition.perspectiveUpIn',
        'transition.perspectiveUpOut' => 'transition.perspectiveUpOut',
        'transition.perspectiveDownIn' => 'transition.perspectiveDownIn',
        'transition.perspectiveDownOut' => 'transition.perspectiveDownOut',
        'transition.perspectiveLeftIn' => 'transition.perspectiveLeftIn',
        'transition.perspectiveLeftOut' => 'transition.perspectiveLeftOut',
        'transition.perspectiveRightIn' => 'transition.perspectiveRightIn',
        'transition.perspectiveRightOut' => 'transition.perspectiveRightOut',
    );
}

/**
 * Animation style options for dropdown
 */
function _s_animation_options($default_options) {
  return array_merge($default_options, _s_animations());
}
add_filter( 'luxe_animation_options', '_s_animation_options' );

/**
 * Add icons to elementor
 */
function _s_add_icons_to_elementor($default_options) {
  $theme_icons = array(
    'icon-mouse' => 'icon-mouse',
    'icon-chevron-up' => 'icon-chevron-up',
    'icon-chevron-down' => 'icon-chevron-down',
    'icon-chevron-left' => 'icon-chevron-left',
    'icon-chevron-right' => 'icon-chevron-right',
    'icon-cross2' => 'icon-cross2',
    'icon-palette' => 'icon-palette',
    'icon-brush2' => 'icon-brush2',
    'icon-desktop' => 'icon-desktop',
    'icon-paper-plane' => 'icon-paper-plane',
    'icon-pencil-ruler' => 'icon-pencil-ruler',
    'icon-arrow-up' => 'icon-arrow-up',
    'icon-arrow-down' => 'icon-arrow-down',
    'icon-arrow-left' => 'icon-arrow-left',
    'icon-arrow-right' => 'icon-arrow-right',
    'icon-menu' => 'icon-menu',
    'icon-telephone' => 'icon-telephone',
    'icon-envelope' => 'icon-envelope',
    'icon-clock3' => 'icon-clock3',
  );
  return array_merge($theme_icons, $default_options);
}
add_filter( 'elementor/icons', '_s_add_icons_to_elementor' );


/**
 * Number of columns per woocommerce row
 */
if (!function_exists('_s_woocommerce_loop_columns')) {
  function _s_woocommerce_loop_columns() {
    return 3;
  }
}
add_filter('loop_shop_columns', '_s_woocommerce_loop_columns');

/**
 * Add data attibutes to body tag
 */
function _s_add_body_atts($atts) {
  // Overlay header scheme
  $offcanvas_header_scheme = get_theme_mod( 'offcanvas_header_scheme', 'light' );
  $atts['data-offcanvas-header-scheme'] = $offcanvas_header_scheme;
  return $atts;
}
add_filter('luxe_body_atts', '_s_add_body_atts', 10, 1);

/**
 * Page links for posts and pages
 */
function _s_page_links() {
  $defaults = array(
    'before'           => '<span class="page-links-label">' . esc_html__( 'Pages:', '_s' ) . '</span>',
    'after'            => '',
    'link_before'      => '',
    'link_after'       => '',
    'next_or_number'   => 'number',
    'separator'        => ' ',
    'nextpagelink'     => '<span class="icon icon-chevron-right"></span>',
    'previouspagelink' => '<span class="icon icon-chevron-left"></span>',
    'pagelink'         => '%',
    'echo'             => 1
  );
  ?>
  <div class="page-links">
    <?php wp_link_pages( $defaults ); ?>
  </div>
  <?php
}

function _s_index_navigation() {
  $pagination = paginate_links( array(
    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
    'format'       => '?paged=%#%',
    'show_all'     => false,
    'prev_next'    => true,
    'prev_text'    => '<span class="icon icon-chevron-left"></span>',
    'next_text'    => '<span class="icon icon-chevron-right"></span>',
    'add_args'     => false,
  ) );
  ?>
  <div class="pagination">
    <?php echo $pagination; ?>
  </div>
  <?php
}

/**
 * Add walker to widget menus
 */
function _s_widget_menu_walker( $args ) {
  return array_merge( $args, array(
      'walker' => new WP_Bootstrap_Navwalker(),
  ) );
}
add_filter( 'wp_nav_menu_args', '_s_widget_menu_walker' );

/**
* Add templates to pages
*/
function _s_add_page_templates($templates) {
  $post_types = get_post_types_by_support( 'elementor' );
  // wp_die(print_r($post_types));
  foreach ( $post_types as $post_type ) {
    $templates['header-template.php'] = esc_html__( 'Header Template', '_s' );
  }
  return $templates;
}
add_filter( "theme_ae_global_templates_templates", '_s_add_page_templates', 20, 4);