<?php

/**
 * Recommended Plugins
 */
function _s_register_required_plugins() {
	/*
	 * Array of plugin arrays
	 */
	$plugins = array(

		array(
			'name'               => esc_html__('ThemeLuxe Post Types', '_s'),
			'slug'               => 'luxe-post-types',
			'source'             => get_template_directory() . '/inc/plugins/luxe-post-types.zip',
			'required'           => true,
			'version'            => '',
			'force_activation'   => false,
			'force_deactivation' => false,
			'external_url'       => '',
			'is_callable'        => '',
		),

		array(
			'name'      => esc_html__('One Click Demo Import', '_s'),
			'slug'      => 'one-click-demo-import',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('Kirki Toolkit', '_s'),
			'slug'      => 'kirki',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('Elementor', '_s'),
			'slug'      => 'elementor',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('AnyWhere Elementor', '_s'),
			'slug'      => 'anywhere-elementor',
			'required'  => false,
		),
		array(
			'name'      => esc_html__('WooCommerce', '_s'),
			'slug'      => 'woocommerce',
			'required'  => false,
		),
		array(
			'name'      => esc_html__('Contact Form 7', '_s'),
			'slug'      => 'contact-form-7',
			'required'  => false,
		),
		
	);

	/*
	 * Configuration settings
	 */
	$config = array(
		'id'           => '_s-tgmpa',
		'default_path' => '',                      
		'menu'         => 'tgmpa-install-plugins',
		'parent_slug'  => 'themes.php',
		'capability'   => 'edit_theme_options',
		'has_notices'  => true,
		'dismissable'  => true,
		'dismiss_msg'  => '',
		'is_automatic' => true,
		'message'      => '',
		/*
		'strings'      => array(
			'page_title'                      => esc_html__( 'Install Required Plugins', 'theme-slug' ),
			'menu_title'                      => esc_html__( 'Install Plugins', 'theme-slug' ),
			// <snip>...</snip>
			'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
		)
		*/
	);

	tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', '_s_register_required_plugins' );