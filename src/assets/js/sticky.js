(function($) {
  "use strict";

  window.initSticky = function() {
	  var windowWidth = $(window).width();
	  if (windowWidth < 768) {
  	  $(".stick-on-scroll").trigger("sticky_kit:detach");
		} else {
			$(".stick-on-scroll").stick_in_parent();
		}
  }

  window.destroySticky = function() {
    $(".stick-on-scroll").trigger("sticky_kit:detach");
  }

  $(window).load(function() {
		$(document.body).trigger("sticky_kit:recalc");
  });

  $(window).resize(function() {
    window.initSticky();
  });

})(jQuery);