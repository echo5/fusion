(function($){
	"use strict";


	var px = true, fn = 'velocity',
		d = document, w = window, s, ww, wh, a = {}, b, particles = [], particle, d, m = Math, mr = function(n){return m.random()*(n||1)};

	window.initParticles = function() {
		winsize();
		var particlesContainer = $('#particles');
		if (particlesContainer.length) {
			if (particlesContainer.hasClass('animated'))
				return;
			particlesContainer.addClass('animated visible');
			var total = particlesContainer.data('count');
			var i = 0;
			var c = 0;
			var colors = particlesContainer.data('colors').split(',');
			$('#particles').empty();
			particles.length = 0;
			if (colors) {  }
			for (i;i<total;i++) {
				var color = colors[c];
				c = c + 1;
				if (c >= colors.length) { c = 0; }
				particles.push($('<i/>').css({top:px?mr(ww):'50%',left:px?mr(ww):'50%',background:color}));
			}
			$('#particles').append(particles);
			for (i in particles) updateParticlePosition(i);
		}
	}

	function updateParticlePosition(n) {
		if (particle = particles[n]) {
			s = mr(10)+4;
			a = {
				left:px?mr(ww-s):(mr(99)+'%'),
				top:px?mr(wh-s):(mr(99)+'%'),
				width:s,
				height:s,
				opacity:mr(.8)+.1
			};
			d = mr(2000)+4900;
			(fn=='animate')?particle.animate(a,d,function(){updateParticlePosition(n)}):particle.velocity(a,d,function(){updateParticlePosition(n)});
		}
	}

	function winsize() {
		ww = $(w).width();
		wh = $(w).height();
	}

	
	// $(w).on('load',function(){
	// 		winsize();
	// });
	$(w).on('resize',function(){
		winsize();
	});
	
})(jQuery);