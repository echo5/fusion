(function($) {
	"use strict";

	window.initProductSingle = function() {
		$( '.wc-tabs-wrapper, .woocommerce-tabs, #rating' ).trigger( 'init' );
		if (typeof $.fn.wc_product_gallery === "function") { 
			$( '.woocommerce-product-gallery' ).each( function() {
				$( this ).wc_product_gallery();
			} );
		}
	}
	
})(jQuery);
