(function($) {
  "use strict";

  var googleMapsLoaded = false;

  window.initMaps = function() {

    var $maps = $('.map-canvas');

    if ($maps.length) {

      if (!googleMapsLoaded) {
          var key = themeConfig.googleMapsApiKey;
          $.getScript("https://maps.google.com/maps/api/js?sensor=true&key=" + key)
          .done(function (script, textStatus) {  
            googleMapsLoaded = true;
            createMaps($maps);
          })
          .fail(function (jqxhr, settings, ex) {
          });
      } else {
        createMaps($maps);
      }

    }
  }

  window.createMaps = function($maps) {
    $maps.each(function() {

      var defaultOptions = {
        scrollwheel: false,
        mapTypeControl: false,
        streetViewControl: false
      };

      var savedOptions = $(this).data('map-options');
      var mapOptions = $.extend( {}, defaultOptions, savedOptions );

      // mapOptions.styles = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#ededed"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f73d38"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#9192a4"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#f73d38"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#eeeeee"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#9192a4"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#f73d38"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"},{"color":"#9192a4"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#f73d38"},{"lightness":17},{"weight":1.2}]}];

      var mapCanvas = $(this).get(0);
      var myLatLong = new google.maps.LatLng(mapOptions.lat, mapOptions.long);
      mapOptions.center = myLatLong;
      var map = new google.maps.Map(mapCanvas, mapOptions);

      if (mapOptions.marker) {
        var marker = new google.maps.Marker({
            position: myLatLong,
            map: map,
            icon: mapOptions.marker
        });
      }
    });
  }

})(jQuery);