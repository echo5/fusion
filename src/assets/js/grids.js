(function($) {
  "use strict";

  window.initGrids = function() {
    var $masonryGrids = $('.luxe-grid-masonry');
    $masonryGrids.imagesLoaded( function() {
    	$masonryGrids.isotope({
    	  itemSelector: '.grid-item',
    	  layoutMode: 'packery',
    	  percentPosition: true,
    	  gutter: 30
    	  // masonry: {
    	  //   // use element for option
    	  //   columnWidth: '.grid-sizer'
    	  // }
    	});
    });
  }

  window.destroyGrids = function() {
    $('.luxe-grid-masonry').isotope('destroy')
  }

  window.initGridHover = function() {
    $('.luxe-grid').each(function() {
      var gridItems = $(this).find('.grid-item');
      var activeGridItems = gridItems.filter('.active');
      gridItems.hover(function() {
        activeGridItems.removeClass('active');
      }, function() {
        activeGridItems.addClass('active');
      });

      // Cut excerpts
      if ($(this).hasClass('post-style-default')) {
        $(this).imagesLoaded( function() {
          showExcerpts();
        });
      }

      $(window).resize(function() {
        showExcerpts();
      });

      function showExcerpts() {
        gridItems.each(function() {
          var overlayHeight = $(this).find('.overlay-inner').height(),
            categoriesHeight = $(this).find('.categories').outerHeight(true),
            readMoreHeight = $(this).find('.read-more').outerHeight(true),
            excerptHeight = $(this).find('.excerpt').outerHeight();
            if ( (overlayHeight - readMoreHeight - categoriesHeight) >= excerptHeight ) {               
              $(this).find('.excerpt').addClass('show');
            } else {
              $(this).find('.excerpt').removeClass('show');
            }
        });
      }
    });
  }


})(jQuery);