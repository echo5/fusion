(function($) {
    "use strict";

	/**
	 * In viewport helper
	 * @return {Boolean} 
	 */
	$.fn.inView = function(buffer){

		buffer = typeof buffer !== 'undefined' ? buffer : 100;
	    var win = $(window);

	    var viewport = {
	        top : win.scrollTop(),
	        left : win.scrollLeft()
	    };
	    viewport.right = viewport.left + win.width() - buffer;
	    viewport.bottom = viewport.top + win.height() - buffer;

	    var bounds = this.offset();
	    bounds.right = bounds.left + this.outerWidth();
	    bounds.bottom = bounds.top + this.outerHeight();

	    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

	};

	/**
	 * Custom animations
	 */
 	$.Velocity.RegisterEffect('transition.moveUp', {
 	    defaultDuration: 400,
 	    calls: [
 		    [ { opacity: [1, 0], translateY: [ 0, 140 ], translateZ: 0 } ]
 	    ],
 	});

 	$.Velocity.RegisterEffect('transition.moveRight', {
 	    defaultDuration: 1000,
 	    calls: [
 		    [ { opacity: [1, 0], translateX: [ '0%', '-100%' ], translateZ: 0 } ]
 	    ],
 	});

	function animate(element, count) {
		var animationName = element.data('animation');
		var animationCount = element.data('animation-loop');
		var animationDuration = element.data('animation-duration');
		var animationDelay = element.data('animation-delay');
		var animationStagger = element.data('animation-stagger');
		element.addClass('luxe-animated');

		// Animate child or self
		var animatedElement = element,
			childElement = false;
		if (element.hasClass('elementor-widget') || element.hasClass('animate-child')) {
			animatedElement = element.find(">:first-child");
			childElement = true;
		}
		if (animationName) {
			animatedElement.velocity(animationName, { 
				stagger: animationStagger,
				delay: animationDelay,
				duration: animationDuration,
				display: "",
				begin: function(elements) {
					if (childElement) {
						$(elements).parent().addClass('visible');
					} else {
					}
				},
				complete: function(elements) {
					count++;
					if (count < animationCount || animationCount === true) {
						animate(element, count);
					} else {
						element.css('transform', '');
					}
				}
			});
		}
	}

	/**
	 *  Animate elements inside viewport
	 */
	function animateInView(elements) {
		var $elementsToAnimate = $();
		elements.each(function() {
			if (!$(this).hasClass('luxe-animated')) {
				if ($(this).inView()) {
					if ($(this).hasClass('animation-stagger')) {
						$elementsToAnimate = $elementsToAnimate.add($(this));
					} else {
						animate($(this), 0);
					}
				}
			}
		});
		if ($elementsToAnimate.length) {
			animate($elementsToAnimate, 0);
		}
	}

	/**
	 * Animate all elements
	 */ 
	window.animateElements = function(elements) {
		elements.each(function() {
			if (!$(this).hasClass('luxe-animated')) {
				animate($(this), 0);
			}
		});
	}
	// Begin animations
	window.startAnimations = function() {
		var animationBlocks = $('.has-animation');
		animateInView(animationBlocks);
		$(window).scroll(function() {
			animateInView(animationBlocks);
		});
	}
	startAnimations();
	$( document ).on( "load", function() {
		startAnimations();
	});

	/**
	 *  Parallax fxn for elements
	 */
	$.fn.parallax = function(momentum, axis) {
		momentum = typeof momentum !== 'undefined' ? momentum : '0.5';
		axis = typeof axis !== 'undefined' ? axis : 'y';
	    var scrollTop = $(window).scrollTop();
	    var offset = this.parent().offset();
	    var moveValue = 0 - Math.round((offset.top - scrollTop) * momentum);
	    // this.css('transform', 'translateY( '+  moveValue +'px)');
	    if (axis === "x") {
	    	this.velocity({
	    		translateX: moveValue + "px",
	    	}, { queue: false, duration: 0 });  
	    } 
	    else {
			this.velocity({
				translateY: moveValue + "px",
			}, { queue: false, duration: 0 });   	
	    }
	};

	function moveParallaxLayers() {
		$('.parallax-layer').each(function(){
			if ($(this).inView(0)) {
				var momentum = $(this).data('parallax-momentum');
				var axis = $(this).data('parallax-axis');
			    $(this).parallax(momentum, axis);
			}
		});
	}

	window.initParallaxLayers = function() {
		moveParallaxLayers();
		$('.parallax-layer').velocity({ opacity: 1 }, { duration: 300});
		$(window).on('scroll', function() {
			moveParallaxLayers();
		});
	}

	initParallaxLayers();

	/**
	 * Portfolio slide in one by one
	 */
	 // $( document ).on( "preloaderHidden", function() {
	 // 	$('#portfolio-grid').find('.grid-item')
	 //    	.velocity("transition.slideUpIn", { stagger: 250 });
	 // });


})(jQuery);