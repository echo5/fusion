(function($) {
  "use strict";

  window.materializeForms = function() {

    $('input, textarea').each(function() {
      if ($(this).val())
        $(this).addClass('has-value');
      else
        $(this).removeClass('has-value');
    });

  	$('input, textarea').blur(function() {
  	  if ($(this).val())
  	    $(this).addClass('has-value');
  	  else
  	    $(this).removeClass('has-value');
  	});

    // Contact Form 7
    $('.wpcf7').find("input[type=text], input[type=email], input[type=tel], textarea").each( function() {
      $(this).parent().addClass('form-group');
      var placeholder = $(this).attr('placeholder');
      if (typeof(placeholder) !== 'undefined' && placeholder.length) {
        $(this).attr('placeholder', '');
        $(this).after('<label>' + placeholder + '</label>');
      }
    });
    $('.wpcf7').find("select").each( function() {
      $(this).parent().addClass('form-group');
      $(this).wrap( '<div class="select-wrapper"></div>' );
    });

    // Woocommerce
    $('.woocommerce .form-row').find("input[type=text], input[type=email], input[type=password], input[type=tel], textarea").each( function() {
      $(this).parent().addClass('form-group');
      var label = $(this).prev('label');
      if (label.length) {
        label.insertAfter($(this));
      }
      var placeholder = $(this).attr('placeholder');
      $(this).attr('placeholder', '');
    });
    $('select.orderby').each( function() {
      if (!$(this).parent().hasClass('select-wrapper')) {
        $(this).wrap( '<div class="form-group"><div class="select-wrapper"></div></div>' );        
      }
    });
  }

	$(document).ready(function() {
		// materializeForms();
	});

})(jQuery);