(function($) {
  "use strict";


  var body = $('body'),
      navbar = $('#navbar'),
      navItems = $('#nav-primary'),
      header = $('.navbar'),
      closeToggler = $('.close-toggler'),
      content = $('#content');

  // Set default header style
  window.setHeaderSchemeData = function() {
    if(body.hasClass('header-light-active')) {
      body.attr('data-header-scheme', 'light');
    }
    if(body.hasClass('header-dark-active')) {
      body.attr('data-header-scheme', 'dark');
    }
  }
  setHeaderSchemeData();

  var sequences = {
    // Overlay
    "overlayIn": [{
      e: navbar,
      p: {
        translateX: ["100%", "0%"]
      },
      o: {
        duration: 500,
        begin: function(elements) { 
        }
      }
    }, {
      e: $('#barba-wrapper'),
      p: {
        translateX: ["50%", "0%"],
      },
      o: {
        duration: 500,
        sequenceQueue: false
      }
    }, {
      e: closeToggler,
      p: {
        opacity: 1,
      },
      o: {
        visibility: 'visible',
        duration: 200,
      }
    }],
    "overlayOut": [{
      e: navbar,
      p: {
        translateX: "0%"
      },
      o: {
        duration: 500,
        begin: function(elements) { 
        }
      }, 
    }, {
      e: $('#barba-wrapper'),
      p: {
        translateX: [0],
      },
      o: {
        duration: 500,
        sequenceQueue: false,
        complete: function() {
          $(this).css({ 'transform' : '' })
        }
      }
    }],
    // Slide
    "slideIn": [{
      e: navbar,
      p: {
        translateY: ["0%", "-100%"]
      },
      o: {
        duration: 500,
      }
    }, {
      e: $('.navbar-bg'),
      p: {
        translateX: ["50%", "0%"],
      },
      o: {
        duration: 500,
        sequenceQueue: false
      }
    }],
    "slideOut": [{
      e: navbar,
      p: {
        translateY: ["-100%", "0%"]
      },
      o: {
        duration: 500
      }, 
    }, {
      e: $('.navbar-bg'),
      p: {
        translateX: [0],
      },
      o: {
        duration: 500,
        sequenceQueue: false
      }
    }],

    // Side Collapsed
    "sideIn": [{
      e: navbar,
      p: {
        translateX: ["0%", "-100%"]
      },
      o: {
        duration: 500,
      }
    }, {
      e: $('#barba-wrapper'),
      p: {
        translateX: "360px",
      },
      o: {
        duration: 500,
        sequenceQueue: false
      }
    }, {
      e: navItems,
      p: {
        opacity: 1,
      },
      o: {
        duration: 300,
      }
    }],
    "sideOut": [{
      e: navItems,
      p: {
        opacity: 0,
      },
      o: {
        duration: 300,
      }
    }, {
      e: navbar,
      p: {
        translateX: ["-100%", "0%"]
      },
      o: {
        duration: 500
      }, 
    }, {
      e: $('#barba-wrapper'),
      p: {
        translateX: [0],
      },
      o: {
        duration: 500,
        sequenceQueue: false,
        complete: function() {
          $(this).css({ 'transform' : '' })
        }
      }
    }],

  };

  // run the sequence on click
  $(".navbar-toggler").on('click', function(event) {
    event.preventDefault();
    var sequence = $(this).data('sequence');
    if (!body.hasClass('navbar-active')) {
      body.addClass('navbar-active');
      var offcanvasScheme = $('body').data('offcanvas-header-scheme');
      $('body').removeClass('header-dark-active header-light-active').addClass('header-' + offcanvasScheme + '-active'); 
      if (sequence.length) {
        $.Velocity.RunSequence(sequences[sequence + 'In']);
      }
    } else {
      body.removeClass('navbar-active');
      var defaultScheme = $('body').data('header-scheme');
      $('body').removeClass('header-dark-active header-light-active').addClass('header-' + defaultScheme +'-active'); 
      if (sequence.length) {
        $.Velocity.RunSequence(sequences[sequence + 'Out']);
      }
    }
  });

})(jQuery);