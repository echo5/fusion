(function($) {
    "use strict";

    window.initAjaxComments = function() {
        var commentform=$('#commentform'); // find the comment form
        commentform.prepend('<div id="comment-status" ></div>'); // add info panel before the form to provide feedback or errors
        var statusDiv=$('#comment-status'); // define the infopanel


        commentform.submit(function(){
            var errors = false;
            commentform.find('input,textarea').each(function() {
                if($(this).hasClass('required') && !$(this).val().length){
                    $(this).parent().addClass('has-error');
                    errors = true;
                } else {
                    $(this).parent().removeClass('has-error');
                }
            });
            if (!errors) {
                var formdata=commentform.serialize();
                statusDiv.html('<div class="alert alert-info">' + comments.processing + '</div>');
                var formurl=commentform.attr('action');
                $.ajax({
                    type: 'post',
                    url: formurl,
                    data: formdata,
                    error: function(XMLHttpRequest, textStatus, errorThrown)
                        {
                            statusDiv.html('<div class="alert alert-danger">' + comments.blank + '</div>');
                        },
                    success: function(data, textStatus){
                        data = data.replace(/<html/gi,'<document');
                        data = data.replace(/html>/gi,'document>');
                        var responseText = $(data).find('p');
                        responseText.find('a').remove();

                        if(data == "success" && textStatus == "success"){
                            statusDiv.html('<div class="alert alert-success" >' + comments.success + '</div>');
                            commentform.find('textarea[name=comment]').val('');
                        } else if(responseText.length) {
                            statusDiv.html('<div class="alert alert-danger"></div>');
                            statusDiv.find('.alert').html(responseText);
                        } else{
                            statusDiv.html('<div class="alert alert-danger" >' + comments.error + '</div>');
                        }
                    }
                });
            } else {
                statusDiv.html('<div class="alert alert-danger">' + comments.required + '</div>');
            }
            return false;
        });
    }

})(jQuery);
