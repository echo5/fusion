( function( $ ) {

	/**
	 *  Last clicked link helper
	 */
	var lastClickEl;
	Barba.Dispatcher.on('linkClicked', function(el) {
	  lastClickEl = el;
	});

	/**
	 * Update Google Analytics
	 */
	Barba.Dispatcher.on('initStateChange', function() {
		if (typeof ga === 'function') {
	    ga('send', 'pageview', location.pathname);
	  }
	});

	/**
	 *  Close menu if open
	 */
	Barba.Dispatcher.on('linkClicked', function(el) {
	  if($('body').hasClass('navbar-active')) {
	  	$('.navbar-toggler').first().click();
	  }
	});

	/**
	 * Transitions
	 */ 

	var transitions = {};
	transitions['FadeTransition'] = Barba.BaseTransition.extend({
	  start: function() {
	    Promise
	      .all([this.newContainerLoading, this.fadeOut()])
	      .then(this.fadeIn.bind(this));
	  },

	  fadeOut: function() {
    	var deferred = Barba.Utils.deferred();
  		var $oldContainer = $(this.oldContainer);

  		$('html')
  		   .velocity('stop')
  		   .velocity('scroll', { duration: 750, offset: 0 });

	    $oldContainer.velocity({ opacity: 0 }, {
	    	complete: function() {
	    		Barba.Dispatcher.trigger('oldPageHidden', $oldContainer);
			    deferred.resolve();
	    	},
	    	delay: 300
    	});

		return deferred.promise;
	  },

	  fadeIn: function() {
	    var _this = this;
	    var $el = $(this.newContainer);

	    $(this.oldContainer).hide();

			Barba.Dispatcher.trigger('showNewPage', $el);
	    $el.css({
	      visibility : 'visible',
	      opacity : 0
	    });

	    // Change body class
	    $('body').attr('class', window.bodyClasses);

	    $el.velocity({ opacity: 1 }, {
	    	duration: 400,
	    	complete: function() {
		      _this.done();
	    	}
	    });
	  }
	});

	transitions['WipeLeftTransition'] = Barba.BaseTransition.extend({
	  start: function() {
	    Promise
	      .all([this.newContainerLoading, this.fadeOut()])
	      .then(this.fadeIn.bind(this));
	  },

	  fadeOut: function() {
    	var deferred = Barba.Utils.deferred();
	    var $oldContainer = $(this.oldContainer);
	    var $loadingScreen = $('<div id="loading-screen" class="loading-screen"></div>').appendTo('body');

	    $('html')
	       .velocity('stop')
	       .velocity('scroll', { duration: 750, offset: 0 });

	    $loadingScreen.velocity({ translateX: ["0%", "100%"] }, {
	    	duration: 900,
	    	complete: function() {
	    		Barba.Dispatcher.trigger('oldPageHidden', $oldContainer);
			    $oldContainer.hide();
			    deferred.resolve();
	    	}
	    })
			return deferred.promise;
	  },

	  fadeIn: function() {
	    var _this = this;
	    var $newContainer = $(this.newContainer);
	    var $loadingScreen = $('#loading-screen');

	    $('body').attr('class', window.bodyClasses);

			Barba.Dispatcher.trigger('showNewPage', $newContainer);			
			$newContainer.css({'visibility': 'visible'});

			$loadingScreen.velocity({ translateX: ["-100%", "0%"] }, {
	    	duration: 900,
	    	delay: 300,
	    	complete: function() {
	    		$loadingScreen.remove();
		      _this.done();
	    	}
	    });

	  }
	});

	/**
	 *	Get transition from link
	 */
	Barba.Pjax.getTransition = function() {

		 // Default transition
	   var transitionObj = transitions[themeConfig.ajaxTransition];

	   return transitionObj;
	};



	Barba.Dispatcher.on('transitionCompleted', function(currentStatus, oldStatus, container) {
	  setHeaderSchemeData();
		// Swap active link
		$('.navbar .menu-item').removeClass('current-menu-item current_page_item active');
		if (!window.location.origin) {
		  window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
		}
		var link = currentStatus.url.split(window.location.origin)[1].substring(1);
		$('.navbar').find('.menu-item a').each(function(){
			if ($(this).attr('href') == '/' + link || $(this).attr('href') == currentStatus.url) {
				$(this).parent().addClass('current-menu-item current_page_item active');
			}
		});
	});

	/**
	 *	Body classes and styleshets on page change
	 */
	var originalFn = Barba.Pjax.Dom.parseResponse;
	Barba.Pjax.Dom.parseResponse = function(response) {
	    response = response.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', response);
	    newStyleSheets = $(response).filter('link[rel="stylesheet"]');
	    newStyleSheets.each(function(){
	    	var styleSheetID = $(this).attr('id');
	    	if (!$('#' + styleSheetID).length) {
	    		$('head').append($(this));
	    	}
	    });
			window.bodyClasses = $(response).filter('notbody').attr('class');
			var adminBar = $(response).find('#wpadminbar');
			if (adminBar.length) {
				$('#wpadminbar').replaceWith(adminBar);
			}
	    return originalFn.apply(Barba.Pjax.Dom, arguments);
	};

	/**
	 *	Prevent admin and offsite links
	 */
	 Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;
	 Barba.Pjax.preventCheck = function(evt, element) {
     if (!Barba.Pjax.originalPreventCheck(evt, element)) {
         return false;
     }
     if (element.href.toLowerCase().indexOf('wp-admin') >= 0) {
         return false;
     }
     if (element.className.indexOf('ajax_add_to_cart') > -1) {
         return false;
     }
     if (element.className.indexOf('no-ajax') > -1) {
         return false;
     }
		 if (element.href.toLowerCase().indexOf('?elementor') >= 0) {
         return false;
     }
     if (/.pdf/.test(element.href.toLowerCase())) {
         return false;
		 }
		 if (element.className.indexOf('elementor-clickable') > -1) {
				 return false;
		 }
	   return true;
	 };

	 /**
	  *  Remove/cleanup old page functions
	  */
	 Barba.Dispatcher.on('oldPageHidden', function($oldContainer) {
	 	var $carousels = $oldContainer.find('.carousels, .slick-slider');
	   $(window).unbind('scroll');
	   destroyCarousels($carousels);
	   destroySticky();
	   // destroyGrids();
	 });

	 function initAllElements() {
 	  initGrids();
 	  initGridHover();
 		materializeForms();
 		initParallaxLayers();
 		startAnimations();
 		initSticky();
 	  initParticles();
    initAjaxComments();
    initProductSingle();
	  initMaps();
	  objectFitImages();
		initCarousels();
		startAnimations();
		initSticky();
	}

 	/**
 	 *  Initiate new page functions
 	 */
	Barba.Dispatcher.on('showNewPage', function(currentStatus, oldStatus, container) {
		initAllElements();
		if (typeof(window.elementorFrontend) !== 'undefined') {
		  window.elementorFrontend.init();
		}
	});

 /**
  * Start barba and init scripts
	*/
	initAllElements();
	if (themeConfig.ajax == 1 && window.self == window.top) {
		Barba.Pjax.start();		
	}

	/**
	 * Run scripts on element ready if Elementor editor active
	 */
	$(document).ready(function() {
		if ( undefined !== window.elementorFrontend && undefined !== window.elementorFrontend.hooks ) {
			window.elementorFrontend.hooks.addAction( 'frontend/element_ready/global', function( $scope ) {
				initAllElements();
			} );
		}
	});


})(jQuery);