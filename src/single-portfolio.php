<?php
/**
 * The template for displaying a single portfolio item
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

	<?php
	while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/portfolio/content', _s_get_portfolio_single_style() );

	endwhile; // End of the loop.
	?>

<?php
get_footer();
