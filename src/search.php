<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package _s
 */

get_header(); ?>


	<?php if ( have_posts() ) : ?>
		<h1 class="page-title mb-5"><?php printf( esc_html__( 'Search Results for: %s', '_s' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
	<?php else: ?>
		<h1 class="page-title mb-5"><?php esc_html_e( 'Nothing Found', '_s' ); ?></h1>
	<?php endif; ?>

	<div class="row">
		<div class="col-md-8 post-style-<?php echo _s_get_post_grid_style(); ?>">
			<?php
			if ( have_posts() ) : ?>

				<?php
				$GLOBALS['image_size'] = '_s-wide';
				/* Start the Loop */
				while ( have_posts() ) : the_post();
				?>
				<div class="grid-item">
				<?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/post/grid', _s_get_post_grid_style() );
				?>
				</div>
				<?php
				endwhile;

				_s_index_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>
		</div>

		<div class="col-md-4">
			<?php
			get_sidebar();
			?>
		</div>
	</div>


	<?php
	get_footer();
