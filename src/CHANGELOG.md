# Change Log
Theme updates, changes, and fixes.

## [1.3.34] - 2018-07-05

### Added
- Elementor 2.1+ get function for page settings

### Removed
- Deprecated get_page functions for settings

## [1.3.33] - 2018-05-07

### Fixed
- Localize blog nav strings
- Localize social strings
- Localize nav strings

## [1.3.32] - 2018-05-05

### Fixed
- Localize "Read more" strings

## [1.3.31] - 2018-05-01

### Fixed
- Page settings hook for Elementor 2.0

## [1.3.30] - 2018-03-26

### Fixed
- Woocommerce gallery on AJAX

## [1.3.29] - 2017-12-21

### Added
- Button filter to add styles to third party buttons
- No ajax class


## [1.3.28] - 2017-12-8

### Fixed
- Placeholder fix for forms without placeholders

## [1.3.27] - 2017-12-5

### Added
- Global date format override option

### Changed
- Default date format

## [1.3.26] - 2017-11-30

### Fixed
- Portfolio carousel/grid taxonomy filter bug causing items not to show

## [1.3.25] - 2017-11-29

### Changed
- Overlay excerpt text visiblity function
- Use textarea instead of WYSIWYG in repeating fields (Elementor bug)

## [1.3.24] - 2017-11-27

### Changed
- Category uses custom portfolio taxonomy for portfolio post types in query options

## [1.3.23] - 2017-11-24

### Changed
- Ellipsis library for post grid excerpt truncation

### Fixed
- Color picker theme options bug

### Removed
- Woocommerce product reviews override

## [1.3.22] - 2017-10-24

### Added
- Submenu color options in theme options
- Support for Woocommerce 3.2.1 templates

### Fixed
- Submenu for side menu on mobile

### Changed
- Dropped jQuery ellipsis in favor Ellipsis.js due to potential error

## [1.3.21] - 2017-10-22

### Fixed
- Escape post meta strings
- Escape comment strings in validation scripts

## [1.3.20] - 2017-10-12

### Fixed
- Lightbox AJAX fix
- Elementor 1.7.11 icon font breaking change

## [1.3.19] - 2017-10-9

### Added
- AJAX transition state for initializing Javascript plugins

### Removed
- Duplicate minified files

### Fixed
- Unclickable select box in floating Elementor toolbar
- Localized strings for Elementor template library
- Javascript error when Elementor disabled

## [1.3.18] - 2017-10-1

### Changed
- Change active menu item for all menus on page during AJAX transition

### Fixed
- Destroy old carousels on AJAX page change

## [1.3.17] - 2017-9-29

### Fixed
- Prevent re-firing Elementor hooks frontend 

## [1.3.16] - 2017-9-27

### Fixed
- Elementor preview script reload for Elementor 1.7.6

## [1.3.15] - 2017-9-26

### Fixed
- Preview AJAX fix for Elementor 1.7.6

## [1.3.14] - 2017-9-26

### Fixed
- Elementor preview editing when AJAX enabled
- Custom Google Maps secure https protocol
- Elementor Template Library insert button missing

## [1.3.13] - 2017-9-20

### Added
- Header override for Elementor Pro
- Header template for custom header schemes
- AJAX current menu item link update for custom headers
- Menu styles for widget menus
- Minified and unminified Javascript files and sourcemaps

### Fixed
- Admin bar on AJAX'd page

### Changed
- Blog header scheme only affects blog post type

## [1.3.12] - 2017-9-11

### Fixed
- Grid style name misspelling

## [1.3.11] - 2017-9-11

### Added
- Template library content
- Library styling

## [1.3.10] - 2017-9-10

### Added
- Elementor custom editor styling
- Elementor library single style
- Elementor library images
- Elementor theme option to unfix/fix editor
- Linear icons for all icon options

### Changed
- Hide empty portfolio details

### Fixed
- Grid width priority for individual sections over site settings

## [1.3.9] - 2017-9-8

### Added
- Anywhere elementor single templates

### Fixed
- Horizontal overflow on Chrome in Windows

## [1.3.8] - 2017-9-8

### Added
- Allow template tags override for easy customization
- Option to enable minified CSS from theme options

### Changed
- Child theme enqueue

## [1.3.7] - 2017-8-29

### Changed
- Default post style to 'featured top' option
- Hidden site description tagline on smaller devices
- Smaller H1 font size on mobile
- Default index width
- Increased padding for mobile menu

## [1.3.6] - 2017-8-25

### Changed
- Page links on elementor pages

### Fixed
- Image grid bug in safari
- Clip text fill inside sliders on Safari
- Transform fixed background bug with overlay navigation
- Product post helper for WooCommerce
- Widget margins in header and footer
- Sticky areas init on AJAX transition
- Fixed ellipsis shorten length for default post type

## [1.3.5] - 2017-8-22

### Added
- CSS unminified version

### Changed
- Max image height in archive featured images
- Disabled footer widget area prior to elementor activation

### Fixed
- Unescaped strings

## [1.3.4] - 2017-8-16

### Added
- Container to non-elementor pages
- Calendar widget styling
- Table styling

### Fixed
- Woocommerce coupon input styling

### Changed
- Prefixed all image sizes
- AE template styling enqueue to use wp_reset_postdata

## [1.3.3] - 2017-8-12

### Added
- Page links
- Default WP widget styling
- Page titles for non-elementor page templates

### Fixed
- Text domain reference in query controls
- Comment spacing on mobile

### Changed
- Default index masonry grid style
- Default single post style
- Changed width on default post index
- Added default navigation styles prior to option activation
- Default nav and brand color
- Default font families and headings


## [1.3.2] - 2017-8-09

### Added
- Post excerpt for grid items without featured image
- Default background for single card style posts

### Fixed
- Shorten excerpts in overlay when too lengthy

### Changed
- Default index masonry grid style
- Default single post style
- Changed width on default post index
- Added default navigation styles prior to option activation
- Default nav and brand color
- Default font families and headings

## [1.3.1] - 2017-8-08

### Fixed
- Escaped html in all localized strings

### Changed
- Index default to masonry grid
- Default single post with sidebar
- Added default navigation styles prior to option activation

## [1.3.0] - 2017-8-08

### Added
- Carousel sync option
- Carousel center mode
- Carousel class name
- Meta query relation
- Page title color setting
- Page body color setting
- Default button styling when Kirki is not activated

### Fixed
- Demo import preview images path
- Stick on scroll classes for portfolio single content

## [1.2.0] - 2017-7-26

### Added
- Multiple meta query controls for post types
- Custom post type grid

### Fixed
- Materialize password form fields

## [1.1.0] - 2017-7-30

### Fixed
- IE10+ Flexbox queries
- Single post theme option